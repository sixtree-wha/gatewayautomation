## Goal ##
Provide SelfService capability to internal API teams - i.e. allow teams to manage their own RAML and self-service the mulesoft component

## Steps ##
- Gateway flows simplified to a proxy 
- Outline repository structure and dependency management 
- Custom policies to simplify gateway proxy

## Content ##
	.
	├── Readme.md   						<-- This file
	├── custom-policies                     <-- Custom Policies
	│   ├── custom-oauth-policy-v1
	│   ├── custom-oauth-policy-v2
	│   └── custom-oauth-policy-v3
	├── design 								<-- Design
	│   ├── LinuxDesign.md
	│   ├── LinuxGWPlatformRedesign.md
	│   ├── RepositoryDesignOptions.md
	│   └── Tasks.md
	└── gw-proxy-reference-architecture     <-- GW Template Ref Arch
	    ├── buildProxy.sh
	    ├── gatewayProxyTemplate-V0
	    └── gatewayProxyTemplate-V1