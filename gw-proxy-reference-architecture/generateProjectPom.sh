# #!/usr/bin/sh


usage ()
{
  echo 'Usage : '
  echo '   generateProjectPom.sh <APIName> <ProjectVersion> ' 
  echo '      - APIName:  API Name without space'
  echo '      - ProjectVersion: '
  echo 'Example: '
  echo '   generateProjectPom.sh CashoutAPI 1.0.0 '
  echo '       - Project: CashoutAPI-gw-proxy is created'
  echo '       - POM is copied into the project' 
  exit
}


apiName=${1:-}
projectVersion=${2:-1.0.0-SNAPSHOT}
apiTemplateVersion=${3:-V1}

if [ "$apiName" = "" ]
then
    usage
fi




GWTemplateProject="gatewayProxyTemplate-${apiTemplateVersion}"
apiName=`echo $apiName | tr '[:upper:]' '[:lower:]'`

echo "Building Gateway proxy for ${apiName} into folder --> ${apiName}-gw-proxy:"
API_PROJECT="${apiName}-gw-proxy"
if [ -a $API_PROJECT ]; then
	rm -r -f $API_PROJECT
fi

echo "...creating GW Proxy project  \"$API_PROJECT\" "
mkdir -p $API_PROJECT

echo "...copying templated files from \"${GWTemplateProject}\" project "
# cp -r gatewayProxyTemplate/ $API_PROJECT/
cp ${GWTemplateProject}/pom.xml $API_PROJECT/

 

# XML Config files
POM_XML=${API_PROJECT}/pom.xml
 
echo " ...replacing @APINAME@ in pom "
sed -i '' -e "s|@APINAME@|${apiName}|g" ${POM_XML}
echo " ...replacing @PROJECTVERSION@ in pom "
sed -i '' -e "s|@PROJECTVERSION@|${projectVersion}|g" ${POM_XML}

echo 'Pom File Created '
ls -ltr $API_PROJECT/
