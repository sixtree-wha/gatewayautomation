# #!/usr/bin/sh


usage ()
{
  echo 'Usage : '
  echo '   buildProxy.sh <APIName> <ContextRoot> <APIVersion> <PORT>  <TemplateVersion>] [<ProjectVersion>] ' 
  echo '      - APIName:  API Name without space'
  echo '      - ContextRoot:  Context Root without the leading or trailing slash, must contain the version if there is one. For example:  "webxlapi" or "/cashout/v1" or "cashcard/eachway/v3"  '
  echo '      - APIVersion : API version for the generated SIT,UAT and PROD property files'  
  echo '      - PORT : Port that this API will listen to '
  echo '      - TemplateVersion: Override the version of the Template used to generate the project'
  echo '      - ProjectVersion: '
  echo 'Example: '
  echo '   buildProxy.sh CashoutAPI cashout/v1 v1 8095 '
  echo '       - Project: CashoutAPI-gw-proxy is created'
  echo '       - Listens on port 8095 and path /cashout/v1/* '
  echo '   buildProxy.sh webxlapi webxlapi v1 8098 '
  echo '       - Project: webxlapi-gw-proxy is created'
  echo '       - Listens on port 8095 and path /webxlapi/* '
  echo '   buildProxy.sh cashcard-eachwayapi cashcard/eachway/v3 v3 8099  '
   echo '      - Project: cashcard-eachwayapi-gw-proxy is created'
  echo '       - Listens on port 8095 and path /cashcard/eachway/v3/* '  
  exit
}


apiName=${1:-}
apiContextRoot=${2:-}
apiVersion=${3:-}
apiPort=${4:-}
apiTemplateVersion=${5:-V1}
projectVersion=${6:-1.0.0-SNAPSHOT}


if [ "$apiName" = "" ]
then
    usage
fi

if [ "$apiContextRoot" = "" ]
then
    usage
fi

if [ "$apiVersion" = "" ]
then
    usage
fi

if [ "$apiPort" = "" ]
then
    usage
fi



GWTemplateProject="gatewayProxyTemplate-${apiTemplateVersion}"
apiName=`echo $apiName | tr '[:upper:]' '[:lower:]'`

echo "Building Gateway proxy for ${apiName} into folder --> ${apiName}-gw-proxy:"
API_PROJECT="${apiName}-gw-proxy"
if [ -a $API_PROJECT ]; then
	rm -r -f $API_PROJECT
fi

echo "...creating GW Proxy project  \"$API_PROJECT\" "
mkdir -p $API_PROJECT

echo "...copying templated files from \"${GWTemplateProject}\" project "
# cp -r gatewayProxyTemplate/ $API_PROJECT/
cp -r ${GWTemplateProject}/* $API_PROJECT/

# COPY THE RAML Into the Project
# Replace this with a different copy operation
mv ${API_PROJECT}/src/main/api/api.raml ${API_PROJECT}/src/main/api/${apiName}.raml


# XML Config files
TEMPLATE_RAML=${API_PROJECT}/src/main/api/${apiName}.raml
POM_XML=${API_PROJECT}/pom.xml
MULE_PROJECT_XML=${API_PROJECT}/mule-project.xml
API_XML=${API_PROJECT}/src/main/app/api.xml
COMMON_FLOWS_XML=${API_PROJECT}/src/main/app/common-flows.xml
CONFIG_XML=${API_PROJECT}/src/main/app/config.xml
LOG4j_XML=${API_PROJECT}/src/main/resources/log4j2.xml

# Property Files - Templates
SIT_PROPS_T=${API_PROJECT}/src/main/resources/api.sit.properties.template
UAT_PROPS_T=${API_PROJECT}/src/main/resources/api.uat.properties.template
PROD_PROPS_T=${API_PROJECT}/src/main/resources/api.prod.properties.template

# Property Files - APINAME.env.properties
SIT_PROPS=${API_PROJECT}/src/main/resources/${apiName}-${apiVersion}.sit.properties
UAT_PROPS=${API_PROJECT}/src/main/resources/${apiName}-${apiVersion}.uat.properties
PROD_PROPS=${API_PROJECT}/src/main/resources/${apiName}-${apiVersion}.prod.properties

echo "...copying env properties"
# Create the SIT, UAT, PROD Properties from template
cp $SIT_PROPS_T  $SIT_PROPS
cp $UAT_PROPS_T  $UAT_PROPS
cp $PROD_PROPS_T  $PROD_PROPS

#echo "...cleaning up template property files"
# Clean up files 
#rm -f ${API_PROJECT}/src/main/resources/*.template

echo " ...replacing @APINAME@ in xml templates with ${apiName} "
# Put the API NAME in the Configs
sed -i '' -e "s|@APINAME@|${apiName}|g" ${POM_XML}
sed -i '' -e "s|@APINAME@|${apiName}|g" ${MULE_PROJECT_XML}
sed -i '' -e "s|@APINAME@|${apiName}|g" ${LOG4j_XML}
sed -i '' -e "s|@APINAME@|${apiName}|g" ${API_XML}
sed -i '' -e "s|@APINAME@|${apiName}|g" ${COMMON_FLOWS_XML}
sed -i '' -e "s|@APINAME@|${apiName}|g" ${CONFIG_XML}
sed -i '' -e "s|@APINAME@|${apiName}|g" ${TEMPLATE_RAML}
 

sed -i '' -e "s|@PROJECTVERSION@|${projectVersion}|g" ${POM_XML}

echo " ...replacing @APINAME@ in property file template with ${apiName} "
# Put the API NAME in the Property files
sed -i '' -e "s|@APINAME@|${apiName}|g" ${SIT_PROPS}
sed -i '' -e "s|@APINAME@|${apiName}|g" ${UAT_PROPS}
sed -i '' -e "s|@APINAME@|${apiName}|g" ${PROD_PROPS}


echo " ...replacing @APIPORT@ in template with ${apiPort} "
# Put the API PORT in the Property files
sed -i '' -e "s|@APIPORT@|${apiPort}|g" ${SIT_PROPS}
sed -i '' -e "s|@APIPORT@|${apiPort}|g" ${UAT_PROPS}
sed -i '' -e "s|@APIPORT@|${apiPort}|g" ${PROD_PROPS}

echo " ...replacing @APICONTEXTROOT@ in template props and raml with ${apiContextRoot} "
# Put the API RESOURCE ROOT in the Property files
sed -i '' -e "s|@APICONTEXTROOT@|${apiContextRoot}|g" ${SIT_PROPS}
sed -i '' -e "s|@APICONTEXTROOT@|${apiContextRoot}|g" ${UAT_PROPS}
sed -i '' -e "s|@APICONTEXTROOT@|${apiContextRoot}|g" ${PROD_PROPS}
sed -i '' -e "s|@APICONTEXTROOT@|${apiContextRoot}|g" ${TEMPLATE_RAML}


echo " ...replacing @APIVERSION@ in template with ${apiVersion} "
# Put the API VERSION in the Property files
sed -i '' -e "s|@APIVERSION@|${apiVersion}|g" ${SIT_PROPS}
sed -i '' -e "s|@APIVERSION@|${apiVersion}|g" ${UAT_PROPS}
sed -i '' -e "s|@APIVERSION@|${apiVersion}|g" ${PROD_PROPS}
sed -i '' -e "s|@APIVERSION@|${apiVersion}|g" ${TEMPLATE_RAML}
sed -i '' -e "s|@APIVERSION@|${apiVersion}|g" ${CONFIG_XML}

echo " ... cleaning template files "
rm -f ${API_PROJECT}/src/main/resources/*.template

echo "mvn clean install -U" > ${API_PROJECT}/build.sh
chmod 775 ${API_PROJECT}/build.sh

echo " ... Done! "

ls -ltr $API_PROJECT

 
