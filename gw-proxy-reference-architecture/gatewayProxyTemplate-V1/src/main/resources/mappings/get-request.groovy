import groovy.json.JsonSlurper
import groovy.json.JsonOutput

def slurper = new JsonSlurper()
def request = payload ? slurper.parseText(payload) : {}
return JsonOutput.toJson(request)
