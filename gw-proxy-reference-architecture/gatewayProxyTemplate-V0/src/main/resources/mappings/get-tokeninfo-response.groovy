import groovy.json.JsonSlurper
import groovy.json.JsonOutput

def slurper = new JsonSlurper()
def response = payload ? slurper.parseText(payload) : {}

// retrun response as map 
return response
