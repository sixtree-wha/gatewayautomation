import groovy.json.JsonOutput

_out = [:]

_out.status = flowVars.statusCode

if(flowVars.customErrorCode)
	_out.code = flowVars.customErrorCode

_out.detail = flowVars.customErrorDetail

def errorsList = new ArrayList()
errorsList.add(_out)

message.setOutboundProperty('content-type', 'application/json')
message.setPayload(JsonOutput.toJson(['errors' : errorsList]))


//if(flowVars.statusCode) {
//	message.setOutboundProperty('http.status', flowVars.statusCode)
//}

return message


