# Design #


## Repository Structure ##
Options for origanising RAML, GW Proxy and Internal Service are outlined below

### Option 1: ###
All the artifacts are bundled inside the project. 

For example for the Web Team this might make sense:

	WHA/Web/web-services/
		 - webxl-api-contract
		   - api.raml
		   - examples
		   - schemas
		   - tests
		      - contract-tests.json

		 - webxl-api-gw-proxy
		   - gw-config.yaml
		   - gw-tests
		      - integration-tests-collection.json
		   - mulesoft-gw-proxy
		      - pom.xml
		      - mule-project.xml
		      - src/main/api
		      - src/main/app

		   - service-webxl-api
		 	  - src
			 	 - WHA.Api
				 - WHA.Api.Capacity.Tests
				 - WHA.Api.Integration.Tests
				 - WHA.Api.Tests
				 - WHA.Web.Common
				 - WHA.Web.Common.Tests
	 

### Option 2: ###
Interface and Implementation are separated and dependencies manage changes. 

This might make more sense for Internal Services with multiple clients

	WHA/Gateway/
		 - cashout-api-contract
		   - api.raml
		   - examples
		   - schemas
		   - tests
		      - contract-tests.json

		 - cashout-api-gw-proxy
		   - gw-config.yaml
		   - gw-tests
		      - integration-tests-collection.json
		   - mulesoft-gw-proxy
		      - pom.xml
		      - mule-project.xml
		      - src/main/api
		      - src/main/app  

	WHA/Services
	 - service-cashout-bet-filter
	 	 - src      
			- WH.CashoutBetFilter.AcceptanceTests
			- WH.CashoutBetFilter.Api
			- WH.CashoutBetFilter.Business
			- WH.CashoutBetFilter.Core
			- WH.CashoutBetFilter.CoreModels
			- WH.CashoutBetFilter.Dependency
			- WH.CashoutBetFilter.Service
			- WH.CashoutBetFilter.Test.Sdk
			- WH.CashoutBetFilter.UnitTests
