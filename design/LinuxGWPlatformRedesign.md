# Install Steps # 
- For Each Node
	- Puppet / OS install / JDK
	- API Install (1 fully chained init job/ bootstrap )	  (TeamCityJob APIGW-Bootstrap)      
	    - JVM Install            <-- Run once                 (TeamCityJob APIGW-Install-JVM)
	    	- Download API Gateway installer
	    	- Download License
	    	- Build Mule Config from template
	    	- Get Server Registration Key
	    	- Extract API Gateway
	    	- Install License 
	    	- Push Config 
	    	- Register server
	    	- Add Server to Group

	    - Properties Install     <-- Run occasionally         (TeamCityJob APIGW-Deploy-App-Properties)
	    	- Download from Artifact Repo
	    	- Push to common path in {env}
	    	- Restart Application

	    - Code Install           <-- Run often				  (TeamCityJob APIGW-Deploy-App-Code)
	    	- Download from Artifact Repo
	    	- Run Maven Anypoint Deploy

	    - Mule Config update      <-- Run occasionally		  (TeamCityJob APIGW-Deploy-New-Wrapper-Config)
	    	- Download from  Repo
	    	- Push to API-JVM config path
	    	- Restart Application

# Team City Jobs #
- TeamCityJob APIGW-Bootstrap
     - Invokes APIGW-Install-JVM, APIGW-Deploy-App-Properties and APIGW-Deploy-App-Code jobs
     - Used to create a new node and launch a jvm 

- TeamCityJob APIGW-Install-JVM
	- Given an API name it unzips the mulesoft installer, registers the server, installs license

- TeamCityJob APIGW-Deploy-App-Properties
	- First step to deploying an application is to deploy the property
	- This job can be run during an env bootstrap or by the SelfService GW Proxy generator to push property files to environments 
  
- TeamCityJob APIGW-Deploy-App-Code
	- Second step to deploying an application 
	- This job can be run during the env boostrap or by the SelfService GW Proxy generator to push the code to the environment

- TeamCityJob APIGW-Deploy-New-Wrapper-Config
	- This is a mule configuration change type event 
	- This requires building a wrapper config from a template
	- This requires pushing the wrapper config to the API JVM config path 
	- This requires restarting the JVM service


# Repository structure #
- WHA/Gateway/
	- api-gateway-platform-config
		- sit
			- vars
		- uat 
			- vars
		- prod 
			- vars

	- api-gateway-application-properties
	    - betsapi-v1
	    	- betsapi-v1.sit.properties 
	    	- betsapi-v1.uat.properties 
	    	- betsapi-v1.prod.properties 

		- cashoutapi-v1
	    	- cashoutapi-v1.sit.properties 
	    	- cashoutapi-v1.uat.properties 
	    	- cashoutapi-v1.prod.properties 

		- accountapi-v1
	    	- accountapi-v1.sit.properties 
	    	- accountapi-v1.uat.properties 
	    	- accountapi-v1.prod.properties 

		- cashcardapi-v1
	    	- cashcardapi-v1.sit.properties 
	    	- cashcardapi-v1.uat.properties 
	    	- cashcardapi-v1.prod.properties 

		- cashcard-dcrmapi-v1
	    	- cashcard-dcrmapi-v1.sit.properties 
	    	- cashcard-dcrmapi-v1.uat.properties 
	    	- cashcard-dcrmapi-v1.prod.properties 

		- cashcardapi-v2
	    	- cashcardapi-v2.sit.properties 
	    	- cashcardapi-v2.uat.properties 
	    	- cashcardapi-v2.prod.properties 

		- fantasyapi-v1
	    	- fantasyapi-v1.sit.properties 
	    	- fantasyapi-v1.uat.properties 
	    	- apfantasyapi-v1.prod.properties 

		- racingapi-v1
	    	- racingapi-v1.sit.properties 
	    	- racingapi-v1.uat.properties 
	    	- racingapi-v1.prod.properties 

		- sportsapi-v1
	    	- sportsapi-v1.sit.properties 
	    	- sportsapi-v1.uat.properties 
	    	- sportsapi-v1.prod.properties 

		- webxlapi-v1
	    	- webxlapi-v1.sit.properties 
	    	- webxlapi-v1.uat.properties 
	    	- webxlapi-v1.prod.properties 	  

		- cashcard-eachwayapi-v1
	    	- cashcard-eachwayapi-v1.sit.properties 
	    	- cashcard-eachwayapi-v1.uat.properties 
	    	- cashcard-eachwayapi-v1.prod.properties 	   	  	